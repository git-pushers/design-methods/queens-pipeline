package chess;

import pipeline.Handler;

import java.util.ArrayList;

// Фильтрация досок
public class BoardsFilter implements Handler<Data> {

    @Override
    public void Execute(Data data) {
        ArrayList<ArrayList<Data.Position>> nextBoards = new ArrayList<>();
        for (ArrayList<Data.Position> board : data.boards) {
            boolean ok = true;
            for (Data.Position p1 : board) {
                for (Data.Position p2 : board) {
                    if (p1.equals(p2)) {
                        continue;
                    }
                    if (p1.x == p2.x || p1.y == p2.y || (Math.abs(p1.x - p2.x) == Math.abs(p1.y - p2.y))) {
                        ok = false;
                        break;
                    }
                }
            }
            if (ok) {
                nextBoards.add(board);
            }
        }

        data.boards = nextBoards;
    }

}
