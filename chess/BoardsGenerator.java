package chess;

import pipeline.Handler;

import java.util.ArrayList;

// BoardsGenerator генерация шахматных досок, с помощью различных комбинаций расстановки фигуры на одной линии
public class BoardsGenerator implements Handler<Data> {

    // Номер вертикальной линии, на которой будет размещатся фигура для генерации досок (x)
    int line;

    public BoardsGenerator(int line) {
        this.line = line;
    }

    @Override
    public void Execute(Data data) {
        ArrayList<ArrayList<Data.Position>> nextBoards = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            for (ArrayList<Data.Position> board : data.boards) {
                ArrayList<Data.Position> newBoard = new ArrayList<>(board);
                newBoard.add(new Data.Position(line - 1, i));
                nextBoards.add(newBoard);
            }
        }
        data.boards = nextBoards;
    }

}
