package chess;

import java.util.ArrayList;

public class Data {
    public ArrayList<ArrayList<Position>> boards;

    public Data(){
        boards = new ArrayList<>();
        boards.add(new ArrayList<Position>());
    }

    public static class Position {

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int x;
        int y;

        @Override
        public boolean equals(Object obj) {
            Position p = (Position) obj;
            return p.x == x && p.y == y;
        }

        @Override
        public String toString() {
            return String.format("%d%s", x+1, letter(y+1));
        }

        private static String letter(int i) {
            return i > 0 && i < 27 ? String.valueOf((char)(i + 64)) : null;
        }
    }
}




