import chess.BoardsFilter;
import chess.BoardsGenerator;
import chess.Data;
import pipeline.Handler;
import pipeline.Pipeline;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        // Чередуем шаги генерации досок с фильтрацией невалидных
        Pipeline<Data> pipeline = new Pipeline<Data>(new Handler[]{
                new BoardsGenerator(1),
                new BoardsFilter(),
                new BoardsGenerator(2),
                new BoardsFilter(),
                new BoardsGenerator(3),
                new BoardsFilter(),
                new BoardsGenerator(4),
                new BoardsFilter(),
                new BoardsGenerator(5),
                new BoardsFilter(),
                new BoardsGenerator(6),
                new BoardsFilter(),
                new BoardsGenerator(7),
                new BoardsFilter(),
                new BoardsGenerator(8),
                new BoardsFilter(),
        });

        Data data = new Data();

        // Запуск пайплайна
        pipeline.Start(data);

        System.out.format("%d solutions\n", data.boards.size());
        for (ArrayList<Data.Position> boards: data.boards){
            StringBuilder sb = new StringBuilder();
            for (Data.Position p: boards){
                sb.append(p.toString()).append(" ");
            }
            System.out.println(sb.toString());
        }
    }



}
