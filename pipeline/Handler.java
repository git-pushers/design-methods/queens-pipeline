package pipeline;

public interface Handler<T> {
    void Execute(T data);
}
