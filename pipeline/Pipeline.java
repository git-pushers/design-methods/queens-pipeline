package pipeline;

// Pipeline ...
public class Pipeline<T> {

    private final Handler<T>[] handlers;

    public Pipeline(Handler<T>[] handlers) {
        this.handlers = handlers;
    }

    public void Start(T data) {
        for (Handler<T> handler : this.handlers) {
            handler.Execute(data);
        }
    }
}
